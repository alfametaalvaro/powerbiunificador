<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class biXusuario extends Model
{
    protected $fillable = [
        'usuario_id', 'bi_id','esta_ativo'
    ];
    protected $dates = [
        'created_at','updated_at'
    ];
    protected $table = 'bisXusuario';
}
