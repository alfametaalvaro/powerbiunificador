<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Usuario;
use JWTAuth;

class e_adm
{
    public function handle($request, Closure $next)
    {
        $dados =(JWTAuth::getToken());
        $Administrador = false;
        if(!$dados == null or !$dados == ''){
            $where = ['token' => $dados];
            $Administrador = Usuario::select('e_adm')->where($where)->first();
        }

        if($Administrador->e_adm == true){
            return $next($request);
        }else{
            return response()->json(['Erro'=>'Ação restrita para Administradores!']);
        }
    }
}
