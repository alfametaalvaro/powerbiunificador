<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use JWTAuth;
use Hash;
use JWTGuard;
use Illuminate\Support\Facades\DB;

class AutorizacaoController extends Controller
{
    public function login(Request $request)
    {
        $usuarioRequest = $request->get('login');
        $senha = $request->get('senha');

        try{
            $usuario = Usuario::where('nome','=', $usuarioRequest)->first();

            if(empty($usuario)){
                return response()->json([
                    'Erro' => 'Usuario não encontrado',
                    'Status'=>0
                ],401);
            }
			if($usuario->esta_ativo == false){
                return response()->json([
                    'Erro' => 'Usuário desativado!',
                    'Status'=>0
                ], 401);
            }
            if(!Hash::check($senha, $usuario->senha)){
                return response()->json([
                    'Erro' => 'Senha invalida!',
                    'Status'=>0
                ], 401);
            }
            $token = JWTAuth::fromUser($usuario);
            $objectToken = JWTAuth::setToken($token);
            $usuario->auth_token = $token;
            $insereToken = DB::connection('pgsql')->insert('UPDATE usuarios set token =  \''.$token.'\' where nome = \''.$usuarioRequest.'\'');
            //$response = ['status'=>true, 'CodUsuario'=>$usuario->CodUsuario, 'token'=> $usuario->auth_token, 'email'=> $usuario->email, 'superuser'=> $usuario->superuser];
            $expiracao = JWTAuth::decode($objectToken->getToken())->get('exp');

            $resposta = [
                'token' => $token,
                'tipo_token' => 'bearer',
                'expira_em' => '30 dias',
                'Status'=>1,
                'CodUsuario'=>$usuario->id,
                'Nome'=>$usuario->nome,
                'e_adm'=>$usuario->e_adm
            ];

            return response()->json($resposta);
        }catch(\Exception $a){
            return response()->json(['Status'=>false,'msg'=>"Banco de dados offline",$a]);
        }
    }

    public function VerificaSeEAdministrador(Request $request){
        $dados = $request->only('token');
        $Administrador = false;
        if(!$dados == null or !$dados == ''){
            $where = ['token' => $dados['token']];
            $Administrador = Usuario::select('e_adm')->where($where)->first();
        }

        if($Administrador->e_adm == true){
            return response()->json(['e_adm'=>true]);
        }else{
            return response()->json(['e_adm'=>false]);
        }
    }

    public function ValidaToken(Request $request){
        $user_ret = false;
		$dados = $request->token;

		if(!$dados == null || !$dados == ''){
			$usuario = Usuario::where('token', $request->token)->get()->first();
			if($usuario != null){
				$user_ret = true;
			}
        }
        if($user_ret){
            return response()->json(['Autenticado'=>$user_ret],200);
        }else{
            return response()->json(['Autenticado'=>$user_ret],401);
        }
    }

    public function authenticate(Request $request){
        try{
            if(!$usuario = JWTAuth::parseToken()->authenticate()){
                return response()->json(['Nao encontrado'],404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['Token expirado'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['Token Invalido'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['Token não informado'], $e->getStatusCode());
        }
        return response()->json(compact('usuario'));
    }

    public function Logout(Request $request){
        JWTAuth::invalidate(JWTAuth::getToken());
        return response([
            'Status'=>true,
            'msg' => 'Logout com sucesso'
        ],200);
    }
	public function autenticaToken(Request $request){
        return response()->json(['Autenticado'=>true]);
    }
}


/*

public function autenticar(Request $request){
        $credenciais = $request -> only('email', 'senha');
        try{
            if(!$token = JWAuth::attempt($credenciais)){
                return response()->json([
                    'error'=>'credenciais invalidas'
                ],400);
            }
        }catch(JWTException $e){
            return response()->json([
                'erro' => 'nao foi possivel criar o token'
            ],500);
        }
        return response()->json(compact('token'));



*/
