<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use PhpParser\Node\Expr\AssignOp\Concat;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\AutorizacaoController;

class UsuarioController extends Controller
{
    public function listar(Request $request)
    {
        $id = $request->get('empresa_id');
        if(is_null($id)){
            return Usuario::all();
        }
        $lista = Usuario::where('empresa_id',$id);
        return $lista->get();

    }

    public function criar_usuario(Request $request)
    {
        //if(AutorizacaoController::VerificaSeEAdministrador($request)){
            $usuario = $request->except('senha');
            $usuario["senha"] = Hash::make($request->get('senha'));
            try{
                $Usuario = Usuario::create($usuario);
                $token = JWTAuth::fromUser($Usuario);
                if($Usuario){
                    return response()->json([
                        'status' => true,
                        'msg' => 'Usuario cadastrado com sucesso'
                    ],200);
                }else{
                    return response()->json([
                        'status' => false,
                        'msg' => 'Usuario nao cadastrado'
                    ]);
                }
            }catch(\Exception $e){

                return response()->json([
                    'status' => false,
                    'msg' => 'Usuario nao cadastrado, contate o suporte.',
                    'erro' => $e
                ],400);
            }

    }

    public function atualizar_usuario(Request $request)
    {
        $Usuario = $request->all();
        $id = $request->get('id');
        if($id == null){
            return response()->json([
                'status' => false,
                'msg' => 'ID do usuario não informado'
            ]);
        }
        try{
            $atualizou = Usuario::where('id', $id)->update($Usuario);
            if($atualizou){
                return response()->json([
                    'status' => true,
                    'msg' => 'Usuario atualizado com sucesso'
                ],200);
            } else{
                return response()->json([
                    'status' => false,
                    'msg' => 'Usuario não atualizado'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'msg' => 'Usuario não atualizado, contate o suporte.',
                'erro' => $e
            ],400);
        }
    }

    public function desativar_usuario(Request $request)
    {
        $Usuario = $request->all();
        $id = $request->get('id');
        if($id == null){
            return response()->json([
                'status' => false,
                'msg' => 'ID do usuario não informado'
            ]);
        }
        try{
            $desativou = Usuario::where('id', $id)->update(['esta_ativo'=>false]);
            if($desativou){
                return response()->json([
                    'status' => true,
                    'msg' => 'Usuario desativado com sucesso'
                ],200);
            } else{
                return response()->json([
                    'status' => false,
                    'msg' => 'Usuario não desativado'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'msg' => 'Usuario não desativado, contate o suporte.',
                'erro' => $e
            ],400);
        }
    }

}
