<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use JWTAuth;
use Hash;
use JWTGuard;
use Illuminate\Support\Facades\DB;

class AutorizacaoController extends Controller
{
    public function login(Request $request)
    {
        $usuarioRequest = $request->Login;
        $senha = $request->Senha;

        try{
            $usuario = Usuario::where('usuario','=', $usuarioRequest)->get();

            if(empty($usuario[0])){
                return response()->json([
                    'Erro' => 'Usuario não encontrado',
                    'Status'=>0
                ],401);
            }
			if($usuario[0]->ativo == 'n'){
                return response()->json([
                    'Erro' => 'Usuário desativado!',
                    'Status'=>0
                ], 401);
            }
            //Tive que desativar a criptografia da senha por que o sistema não vai conseguir criptografa-la
            if(($senha != $usuario[0]->senha)){
                return response()->json([
                    'Erro' => 'Senha invalida!',
                    'Status'=>0
                ], 401);
            }
            /*if(!Hash::check($senha, $usuario[0]->senha)){
                return response()->json([
                    'Erro' => 'Senha invalida!',
                    'Status'=>0
                ], 401);
            }*/
            $token = JWTAuth::fromUser($usuario[0]);
            $objectToken = JWTAuth::setToken($token);
            $usuario->auth_token = $token;
            $insereToken = DB::connection('pgsql')->insert('UPDATE usuariosweb set auth_token =  \''.$token.'\' where usuario = \''.$usuarioRequest.'\'');
            //$response = ['status'=>true, 'CodUsuario'=>$usuario->CodUsuario, 'token'=> $usuario->auth_token, 'email'=> $usuario->email, 'superuser'=> $usuario->superuser];
            $expiracao = JWTAuth::decode($objectToken->getToken())->get('exp');

            $resposta = [
                'token_acesso' => $token,
                'tipo_token' => 'bearer',
                'expira_em' => '30 dias',
                'Status'=>1,
                'CodUsuario'=>$usuario[0]->id,
                'CodUsuarioClassificadorSistema'=>$usuario[0]->codclassificador,
                'NomeUsuario'=>$usuario[0]->nome,
                'Superuser'=>$usuario[0]->superuser
            ];

            return response()->json($resposta);
        }catch(\Exception $a){
            return response()->json(['Status'=>false,'msg'=>"Banco de dados offline",$a]);
        }
    }

    public function VerificaSeEAdministrador(Request $request){
        $dados = $request->only('token');
        $Administrador = false;
        if(!$dados == null or !$dados == ''){
            $where = ['auth_token' => $dados['token']];
            $Administrador = Usuario::select('superuser')->where($where)->get();
        }

        if($Administrador[0]->superuser == true){
            return response()->json(['superuser'=>true]);
        }else{
            return response()->json(['superuser'=>false]);
        }
    }

    public function ValidaToken(Request $request){
        $user_ret = false;
		$dados = $request->token;

		if(!$dados == null || !$dados == ''){
			$usuario = Usuario::where('auth_token', $request->token)->get()->first();
			if($usuario != null){
				$user_ret = true;
			}
        }
        if($user_ret){
            return response()->json(['Autenticado'=>$user_ret],200);
        }else{
            return response()->json(['Autenticado'=>$user_ret],401);
        }
    }

    public function authenticate(Request $request){
        try{
            if(!$usuario = JWTAuth::parseToken()->authenticate()){
                return response()->json(['Nao encontrado'],404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['Token expirado'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['Token Invalido'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['Token não informado'], $e->getStatusCode());
        }
        return response()->json(compact('usuario'));
    }

    public function Logout(Request $request){
        JWTAuth::invalidate(JWTAuth::getToken());
        return response([
            'Status'=>true,
            'msg' => 'Logout com sucesso'
        ],200);
    }
	public function autenticaToken(Request $request){
        return response()->json(['Autenticado'=>true]);
    }
}


/*

public function autenticar(Request $request){
        $credenciais = $request -> only('email', 'senha');
        try{
            if(!$token = JWAuth::attempt($credenciais)){
                return response()->json([
                    'error'=>'credenciais invalidas'
                ],400);
            }
        }catch(JWTException $e){
            return response()->json([
                'erro' => 'nao foi possivel criar o token'
            ],500);
        }
        return response()->json(compact('token'));



*/
