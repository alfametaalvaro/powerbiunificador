<?php

namespace App\Http\Controllers;

use App\BI;
use Request;
//use Illuminate\Support\Facades\Request;

class BIController extends Controller
{

    public function index()
    {
        return BI::all();
    }


    public function criar_bi(Request $request)
    {
        $BI = $request::all();
        try{
            $salvou=BI::create($BI);
            if($salvou){
                return response()->json([
                    'status'=>true,
                    'msg'=>'BI cadastrado com sucesso'
                ]);
            } else{
                return response()->json([
                    'status'=>false,
                    'msg'=>'BI não cadastrado'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status'=>false,
                'msg'=>'BI não cadastrado, contate o suporte.',
                'erro'=>$e
            ],400);
        }
    }



    public function atualizar_bi(Request $request)
    {
        $BI=$request::all();
        $id=$request::get('id');
        try{
            $atualizou=BI::where('id',$id)->update($BI);
            if($atualizou){
                return response()->json([
                    'status'=>true,
                    'msg'=>'BI atualizado com sucesso'
                ]);
            } else{
                return response()->json([
                    'Status'=>false,
                    'msg'=>'BI não atualizado'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status'=>false,
                'msg'=>'BI não atualizado, contate o suporte.',
                'erro'=>$e
            ],400);
        }
    }

    public function desativar_bi(Request $request)
    {
        $id = $request::get('id');
        if($id == null){
            return response()->json([
                'status' => false,
                'msg' => 'ID do BI não informado'
            ]);
        }
        try{
            $desativou = BI::where('id', $id)->update(['esta_ativo'=>false]);
            if($desativou){
                return response()->json([
                    'status' => true,
                    'msg' => 'BI desativado com sucesso'
                ],200);
            } else{
                return response()->json([
                    'status' => false,
                    'msg' => 'BI não desativado'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'msg' => 'BI não desativado, contate o suporte.',
                'erro' => $e
            ],400);
        }
    }

}
