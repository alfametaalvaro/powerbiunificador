<?php

namespace App\Http\Controllers;

use App\biXusuario;
use App\Usuario;
use App\Http\Controllers\AutorizacaoController;
use Request;

class biXusuarioController extends Controller
{

    public function listar(Request $request)
    {
        $token = $request::get('token');
        $id = Usuario::where('token','=',$token)->first();

        if(is_null($id)){
            return biXusuario::all();
        }
        $lista = biXusuario::where('usuario_id',$id);
        return $lista->get();
    }

    public function criar_biXusuario(Request $request)
    {
        $tabela = $request::all();
        try{
            $salvou = biXusuario::create($tabela);
            if($salvou){
                return response()->json([
                    'status' => true,
                    'msg' => 'biXusuario criado com sucesso'
                ],200);
            }else{
                return response()->json([
                    'status' => false,
                    'msg' => 'biXusuario nao criado'
                ]);
            }
        }catch(\Exception $e){
                return response()->json([
                    'status'=> false,
                    'msg' => 'biXusuario não criado, contate o suporte',
                    'erro' => $e
                ],400);
        }
    }


    public function atualizar_biXusuario(Request $request)
    {
        $tabela = $request::all();
        $id = $request::get('id');
        try{
            $atualizou = biXusuario::where('id', $id)->update($tabela);
            if($atualizou){
                return response()->json([
                    'status' => true,
                    'msg' => 'biXusuario atualizado com sucesso'
                ],200);
            }else{
                return response()->json([
                    'status' => false,
                    'msg' => 'biXusuario nao atualizado'
                ]);
            }
        }catch(\Exception $e){
                return response()->json([
                    'status'=> false,
                    'msg' => 'biXusuario não atualizado, contate o suporte',
                    'erro' => $e
                ],400);
        }
    }

    public function desativar_biXusuario(Request $request)
    {
        $id = $request::get('id');
        if(is_null($id)){
            return response()->json([
                'status' => false,
                'mensagem' => 'ID não informado'
            ]);
        }
        try{
            $atualizou = biXusuario::where('id', $id)->update(['esta_ativo'=>false]);
            if($atualizou){
                return response()->json([
                    'status' => true,
                    'msg' => 'biXusuario desativado com sucesso'
                ],200);
            }else{
                return response()->json([
                    'status' => false,
                    'msg' => 'biXusuario nao desativado'
                ]);
            }
        }catch(\Exception $e){
                return response()->json([
                    'status'=> false,
                    'msg' => 'biXusuario não desativado, contate o suporte',
                    'erro' => $e
                ],400);
        }
    }

}
