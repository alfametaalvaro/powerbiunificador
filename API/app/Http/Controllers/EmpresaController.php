<?php

namespace App\Http\Controllers;

use App\Empresa;
use Request;

class EmpresaController extends Controller
{
    public function listar()
    {
        return Empresa::all();
    }

    public function criar_empresa(Request $request)
    {
        $Empresa = $request::all();
        try{
            $salvou = Empresa::create($Empresa);
            if($salvou){
                return response()->json([
                    'status'=>true,
                    'msg'=> 'Empresa cadastrada com sucesso'
                ]);
            }else{
                return response()->json([
                    'status'=>false,
                    'msg'=> 'Empresa nao cadastrada'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status'=> false,
                'mensagem' => 'Empresa não cadastrada, contate o suporte.',
                'erro'=>$e
            ],400);
        }
    }

    public function atualizar_empresa(Request $request)
    {
        $Empresa=$request::all();
        $id = $request::get('id');
        if($id==null){
            return response()->json([
                'status'=>false,
                'mensagem'=>'ID da empresa não informado'
            ]);
        }
        try{
            $atualizou=Empresa::where('id',$id)->update($Empresa);
            if($atualizou){
                return response()->json([
                    'status'=>true,
                    'mensagem'=>'Empresa atualizada com sucesso'
                ]);
            } else{
                return response()->json([
                    'status'=>false,
                    'mensagem'=>'Empresa não atualizada'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status'=>false,
                'mensagem'=>'Empresa não atualizada, contate o suporte.',
                'erro'=>$e
            ],400);
        }
    }

    public function desativar_empresa(Request $request)
    {
        $id = $request::get('id');
        if($id==null){
            return response()->json([
                'status'=>false,
                'mensagem'=>'ID da empresa não informado'
            ]);
        }
        try{
            $desativou=Empresa::where('id',$id)->update(['esta_ativo'=>false]);
            if($desativou){
                return response()->json([
                    'status'=>true,
                    'mensagem'=>'Empresa desativada com sucesso'
                ]);
            } else{
                return response()->json([
                    'status'=>false,
                    'mensagem'=>'Empresa não desativada'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'status'=>false,
                'mensagem'=>'Empresa não desativada, contate o suporte.',
                'erro'=>$e
            ],400);
        }
    }
}
