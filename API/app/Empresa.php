<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
		'nome', 'logo','esta_ativo'
	];
	protected $dates = [
		'created_at','updated_at'
	];
	protected $table = 'empresas';

	public function funcionarios()
	{
		return $this->hasMany('App\Usuraio');
    }
    public function bi()
	{
		return $this->hasMany('App\BI');
    }

}
