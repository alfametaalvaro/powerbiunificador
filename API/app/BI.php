<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BI extends Model
{
    protected $fillable = [
        'nome', 'url','icone','esta_ativo','empresa_id'
    ];
    protected $dates = [
        'created_at','updated_at'
    ];
    protected $table = 'bis';

    function empresa()
	{
		return $this->belongsTo('App\Empresa');
    }

}
