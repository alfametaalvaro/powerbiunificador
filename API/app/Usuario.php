<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Usuario extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'id','nome', 'email','empresa_id','senha','e_adm','esta_ativo'
    ];

    protected $hidden = [
        'senha', 'token',
    ];
    protected $table = 'usuarios';

    protected $date = [
        'created_at','updated_at'
    ];
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function getJWTIdentifier()
    {
        return $this -> getKey();
    }

    public function getJWTCustomClaims()
    {
        return[];
    }

}
