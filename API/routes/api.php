<?php

use Illuminate\Http\Request;

    //Login
    Route::post('login', 'AutorizacaoController@login');

    //Protegidos por Login
    Route::group(['middleware'=>['jwt.verify']],function()
    {
        //Route::get('user', 'AutorizacaoController@authenticate');
        Route::get('usuario', 'UsuarioController@listar');
        //Somente Admins
        //Route::group(['middleware'=>'e_adm'],function()
        //{
            //Usuario
            Route::post('usuario', 'UsuarioController@criar_usuario');
            Route::put('usuario', 'UsuarioController@atualizar_usuario');
            Route::delete('usuario', 'UsuarioController@desativar_usuario');
            //Route::get('usuario', 'UsuarioController@listar');

            //BI X Usuario
            Route::post('bixusuario','biXusuarioController@criar_biXusuario');
            Route::put('bixusuario','biXusuarioController@atualizar_biXusuario');
            Route::delete('bixusuario','biXusuarioController@desativar_biXusuario');
            Route::get('bixusuario','biXusuarioController@listar');

            //Empresa
            Route::post('empresa','EmpresaController@criar_empresa');
            Route::put('empresa','EmpresaController@atualizar_empresa');
            Route::delete('empresa','EmpresaController@desativar_empresa');
            Route::get('empresa','EmpresaController@listar');

            //BI
            Route::post('bi','BIController@criar_bi');
            Route::put('bi','BIController@atualizar_bi');
            Route::delete('bi','BIController@desativar_bi');
            Route::get('bi', 'BIController@listar');

    });




