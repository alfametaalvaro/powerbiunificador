<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaBisXusuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bisXusuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('usuario_id');
            $table->foreign('usuario_id')
                ->references('id')
                ->on('usuarios');
            $table->bigInteger('bi_id');
            $table->foreign('bi_id')
                ->references('id')
                ->on('bis');
            $table->boolean('esta_ativo')->default(true);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bisXusuario');
    }
}
