<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;

class CriarTabelaUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome',100)->unique();
            $table->string('email',60)->unique();
            $table->string('senha',400);
            $table->string('token',500)->nullable(true);
            $table->boolean('esta_ativo')->default(true);
            $table->boolean('e_adm')->default(false);
            $table->bigInteger('empresa_id');
            $table->foreign('empresa_id')
                ->references('id')
                ->on('empresas');
            $table->timestamps();
        });
        /*DB::statement(
            "insert into usuarios(nome,senha,superuser,primeirologin,nome)
                           values('administrador','".\Hash::make('1234')."',true,false,\'Administrador\')"
                                );*/

        DB::table('usuarios')->insert([
            'nome' => 'Administrador',
            'email' => ' ',
            'senha'=>Hash::make('1234'),
            'e_adm'=>true,
            'empresa_id'=>1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
