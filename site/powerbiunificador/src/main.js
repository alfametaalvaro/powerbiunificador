import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Login from './views/Login.vue'
import BIs from './views/BIs.vue'
import Administrar from './views/Administrar'
import Empresas from './views/Empresas'
import Store from '@/components/Store'
import './quasar'

Vue.config.productionTip = false
Vue.component(Login)
Vue.component(BIs)
Vue.component(Administrar)
Vue.component(Empresas)
Vue.component(Store)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
