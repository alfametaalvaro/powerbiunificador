import Vue from 'vue'
import VueRouter from 'vue-router'
import empresas from '../views/Empresas'
import Administrar from '@/views/Administrar'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path:'/empresa',
    name:'cadastroEmpresas',
    component:empresas
  },
  {
    path:'/administrar',
    name:'painelAdministrar',
    component:Administrar
  },
]

const router = new VueRouter({
  routes
})

export default router
