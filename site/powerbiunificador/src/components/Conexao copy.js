var servidor = "192.168.0.22:8084"
  
export function get(endereco){
  return new Promise((resolve,reject) =>{
    var request = new XMLHttpRequest();
    request.open("GET", servidor + "/" + endereco, true);
    request.setRequestHeader("Content-Type", "Application/json");
    try{
      request.send();
      request.onreadystatechange = function(){
        if (request.readyState == request.DONE){
          if (request.status == 200){
            resolve(request.response);
          }
          reject(request.response);
        }
      };
    }catch(err){
      reject(err);
    }
  });
}

export function post(endereco, objeto){
  console.log(endereco);
  return new Promise((resolve, reject)=>{
    var request = new XMLHttpRequest();
    var json = JSON.stringify(objeto);
    //var token = cookies.getCookie("tk");
    //if (protocolo == "http:"){
      request.open("POST",servidor + "/api/" + endereco, true);
    //}
    try{
      request.send(json);
      request.onreadystatechange = function(){
        if(request.readyState == request.DONE){
          if(request.status == 200){
            resolve(request.response);            
          } else{
            reject(request.response);
          }
        }
      }
    } catch(e){
      reject(e);
    }
  });
}


