
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

export default new Vuex.Store ({  
    state:{
      logado: false,
      administrador: false,
      empresa: 'Alfa Meta',
    },
    mutations:{
      logar(state){
        state.logado = true;
      },
      administrar(state){
        state.administrador = true;
      },
      deslogar(state){
        state.logado = false;
        state.admnistrador = false;
      },
      troca_empresa(state, novaempresa){
        state.empresa = novaempresa
      }
    }
})



// export default new Vue({
//   data(){
//     return{
      
//     }
//   },
//   methods:{
//     logar(){this.logado = true},
//     administrar(){ this.adm = true},
//   },
// })