import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Login from './views/Login.vue'
import BIs from './views/BIs.vue'
import Administrar from './views/Administrar'
import Empresas from './views/Empresas'

Vue.config.productionTip = false
Vue.component('Login', Login)
Vue.component('BIs', BIs)
Vue.component('Administrar', Administrar)
Vue.component('Empresas', Empresas)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
